package com.albert_motos.aoc2021;

import java.util.List;

public class SonarSweepImpl implements SonarSweep {

    @Override
    public int countDepthMeasurementIncreases(List<Integer> depthMeasurements) {
        int result = 0;
        int i = 1;

        while (i < depthMeasurements.size()) {
            if (depthMeasurements.get(i - 1) < depthMeasurements.get(i)) {
                result++;
            }

            i++;
        }

        return result;
    }

    @Override
    public int countDepthMeasurementSumTripletIncreases(List<Integer> depthMeasurements) {
        int result = 0;
        int i = 3;

        while (i < depthMeasurements.size()) {
            if (depthMeasurements.get(i - 3) < depthMeasurements.get(i)) {
                result++;
            }

            i++;
        }

        return result;
    }
}
