package com.albert_motos.aoc2021;

import java.util.List;
import java.util.stream.Stream;

public interface SonarSweep {

    /**
     * Counts the number of times a measurement increases from previous measurement.
     *
     * @param depthMeasurements {@link Stream} of depth measurements.
     * @return number of times a measurement increases from previous measurement.
     */
    int countDepthMeasurementIncreases(List<Integer> depthMeasurements);

    /**
     * Counts the number of times the sum of a triplet of measurement increases from previous sum.
     *
     * @param depthMeasurements {@link Stream} of depth measurements.
     * @return number of times the sum of a triplet of measurement increases from previous sum.
     */
    int countDepthMeasurementSumTripletIncreases(List<Integer> depthMeasurements);
}
