package com.albert_motos.aoc2021;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toUnmodifiableList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SonarSweepImplTest {

    private SonarSweep sonarSweep;

    @BeforeEach
    void setUp() {
        sonarSweep = new SonarSweepImpl();
    }

    @DisplayName("GIVEN the sample dataset, WHEN countDepthMeasurementIncreases, THEN result is the expected")
    @Test
    void countDepthMeasurementIncreasesWithSampleDataset() {
        // Given
        List<Integer> dataset = givenSampleDataset();

        // When
        int actual = sonarSweep.countDepthMeasurementIncreases(dataset);

        // Then
        assertEquals(7, actual);
    }

    @DisplayName("GIVEN a dataset, WHEN countDepthMeasurementIncreases(), THEN result is the expected")
    @Test
    void countDepthMeasurementIncreasesWithDataset() {
        // Given
        List<Integer> dataset = givenDataset();

        // When
        int actual = sonarSweep.countDepthMeasurementIncreases(dataset);

        // Then
        assertEquals(1713, actual);
    }

    @DisplayName("GIVEN the sample dataset, WHEN countDepthMeasurementIncreases(), THEN result is the expected")
    @Test
    void countDepthMeasurementSumTripletIncreasesWithSampleDataset() {
        // Given
        List<Integer> dataset = givenSampleDataset();

        // When
        int actual = sonarSweep.countDepthMeasurementSumTripletIncreases(dataset);

        // Then
        assertEquals(5, actual);
    }

    @DisplayName("GIVEN a dataset, WHEN countDepthMeasurementSumTripletIncreases(), THEN result is the expected")
    @Test
    void countDepthMeasurementSumTripletIncreasesWithDataset() {
        // Given
        List<Integer> dataset = givenDataset();

        // When
        int actual = sonarSweep.countDepthMeasurementSumTripletIncreases(dataset);

        // Then
        assertEquals(1734, actual);
    }

    private List<Integer> givenSampleDataset() {
        return Arrays.asList(199, 200, 208, 210, 200, 207, 240, 269, 260, 263);
    }

    private List<Integer> givenDataset() {
        try (Stream<String> strings = Files.lines(Paths.get("src/test/resources/input.dataset"),
                StandardCharsets.UTF_8)) {
            return strings.map(Integer::valueOf).collect(toUnmodifiableList());
        } catch (IOException ignored) {
        }

        return null;
    }
}