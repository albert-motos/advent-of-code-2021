package com.albert_motos.aoc2021;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Segment {

    private final Coordinate source;
    private final Coordinate destination;

    public Segment(Coordinate source, Coordinate destination) {
        this.source = source;
        this.destination = destination;
    }

    public List<Coordinate> getOrthogonalCoordinates() {
        if (source.getX() != destination.getX() && source.getY() != destination.getY()) {
            return Collections.emptyList();
        }

        List<Coordinate> coordinates = new ArrayList<>();
        coordinates.add(source);

        if (source.getX() == destination.getX()) {
            if (source.getY() > destination.getY()) {
                List<Coordinate> yAxisCoordinates = IntStream.range(destination.getY() + 1, source.getY())
                        .mapToObj(y -> new Coordinate(source.getX(), y))
                        .collect(Collectors.toList());
                Collections.reverse(yAxisCoordinates);
                coordinates.addAll(yAxisCoordinates);
            } else {
                coordinates.addAll(IntStream.range(source.getY() + 1, destination.getY())
                        .mapToObj(y -> new Coordinate(source.getX(), y))
                        .collect(Collectors.toList()));
            }
        } else if (source.getY() == destination.getY()) {
            if (source.getX() > destination.getX()) {
                List<Coordinate> xAxisCoordinates = IntStream.range(destination.getX() + 1, source.getX())
                        .mapToObj(x -> new Coordinate(x, source.getY()))
                        .collect(Collectors.toList());
                Collections.reverse(xAxisCoordinates);
                coordinates.addAll(xAxisCoordinates);
            } else {
                coordinates.addAll(IntStream.range(source.getX() + 1, destination.getX())
                        .mapToObj(x -> new Coordinate(x, source.getY()))
                        .collect(Collectors.toList()));
            }
        }
        coordinates.add(destination);

        return coordinates;
    }

    public List<Coordinate> getCoordinates() {
        if (source.getX() == destination.getX() || source.getY() == destination.getY()) {
            return getOrthogonalCoordinates();
        }

        int slope = (destination.getY() - source.getY()) / (destination.getX() - source.getX());
        if (Math.abs(slope) != 1) {
            return Collections.emptyList();
        }

        List<Coordinate> coordinates = new ArrayList<>();
        coordinates.add(source);

        if (slope == 1) {
            if (source.getX() < destination.getX()) {
                coordinates.addAll(generateDiagonalCoordinates(1, 1));
            } else {
                coordinates.addAll(generateDiagonalCoordinates(-1, -1));
            }
        } else {
            if (source.getX() < destination.getX()) {
                coordinates.addAll(generateDiagonalCoordinates(1, -1));
            } else {
                coordinates.addAll(generateDiagonalCoordinates(-1, 1));
            }
        }

        return coordinates;
    }

    private List<Coordinate> generateDiagonalCoordinates(int xIncrement, int yIncrement) {
        List<Coordinate> coordinates = new ArrayList<>();
        Coordinate coordinate = source;
        while (!coordinate.equals(destination)) {
            coordinate = new Coordinate(coordinate.getX() + xIncrement, coordinate.getY() + yIncrement);
            coordinates.add(coordinate);
        }

        return coordinates;
    }

    @Override
    public String toString() {
        return "Segment{" +
                "source=" + source +
                ", destination=" + destination +
                '}';
    }
}
