package com.albert_motos.aoc2021;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HydrothermalVentureImpl implements HydrothermalVenture {

    @Override
    public int getOverlappedOrthogonalSegmentAmount(List<Segment> segments) {
        Map<Coordinate, Integer> collisionsByCoordinate = new HashMap<>();

        for (Segment segment : segments) {
            for (Coordinate coordinate : segment.getOrthogonalCoordinates()) {
                collisionsByCoordinate.compute(coordinate, (key, value) -> value == null ? 1 : value + 1);
            }
        }

        return collisionsByCoordinate.entrySet()
                .stream()
                .filter(entry -> entry.getValue() > 1)
                .mapToInt(entry -> 1)
                .sum();
    }

    @Override
    public int getOverlappedSegmentAmount(List<Segment> segments) {
        Map<Coordinate, Integer> collisionsByCoordinate = new HashMap<>();

        for (Segment segment : segments) {
            for (Coordinate coordinate : segment.getCoordinates()) {
                collisionsByCoordinate.compute(coordinate, (key, value) -> value == null ? 1 : value + 1);
            }
        }

        return collisionsByCoordinate.entrySet()
                .stream()
                .filter(entry -> entry.getValue() > 1)
                .mapToInt(entry -> 1)
                .sum();
    }
}
