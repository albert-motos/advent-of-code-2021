package com.albert_motos.aoc2021;

import java.util.List;

public interface HydrothermalVenture {

    /**
     * Obtains the amount of overlapped orthogonal coordinates.
     *
     * @param segments {@link List} of Segment.
     * @return amount of overlapped orthogonal coordinates.
     */
    int getOverlappedOrthogonalSegmentAmount(List<Segment> segments);

    /**
     * Obtains the amount of overlapped coordinates.
     *
     * @param segments {@link List} of Segment.
     * @return amount of overlapped coordinates.
     */
    int getOverlappedSegmentAmount(List<Segment> segments);
}
