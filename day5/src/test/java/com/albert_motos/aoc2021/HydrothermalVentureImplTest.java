package com.albert_motos.aoc2021;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toUnmodifiableList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class HydrothermalVentureImplTest {

    private HydrothermalVenture hydrothermalVenture;

    @BeforeEach
    void setUp() {
        hydrothermalVenture = new HydrothermalVentureImpl();
    }

    @DisplayName("GIVEN the sample dataset, WHEN getOverlappedOrthogonalSegmentAmount(), THEN result is the expected")
    @Test
    void getOverlappedOrthogonalSegmentAmountWithSampleDataset() {
        // Given
        List<Segment> segments = givenSampleDataset();

        // When
        int actual = hydrothermalVenture.getOverlappedOrthogonalSegmentAmount(segments);

        // Then
        assertEquals(5, actual);
    }

    @DisplayName("GIVEN a dataset, WHEN getOverlappedOrthogonalSegmentAmount(), THEN result is the expected")
    @Test
    void getOverlappedOrthogonalSegmentAmountWithDataset() {
        // Given
        List<Segment> segments = givenDataset();

        // When
        int actual = hydrothermalVenture.getOverlappedOrthogonalSegmentAmount(segments);

        // Then
        assertEquals(6397, actual);
    }

    @DisplayName("GIVEN the sample dataset, WHEN getOverlappedSegmentAmount(), THEN result is the expected")
    @Test
    void getOverlappedSegmentAmountWithSampleDataset() {
        // Given
        List<Segment> segments = givenSampleDataset();

        // When
        int actual = hydrothermalVenture.getOverlappedSegmentAmount(segments);

        // Then
        assertEquals(12, actual);
    }

    @DisplayName("GIVEN a dataset, WHEN getOverlappedSegmentAmount(), THEN result is the expected")
    @Test
    void getOverlappedSegmentAmountWithDataset() {
        // Given
        List<Segment> segments = givenDataset();

        // When
        int actual = hydrothermalVenture.getOverlappedSegmentAmount(segments);

        // Then
        assertEquals(22335, actual);
    }

    private List<Segment> givenSampleDataset() {
        List<String> inputs = Arrays.asList("0,9 -> 5,9",
                "8,0 -> 0,8",
                "9,4 -> 3,4",
                "2,2 -> 2,1",
                "7,0 -> 7,4",
                "6,4 -> 2,0",
                "0,9 -> 2,9",
                "3,4 -> 1,4",
                "0,0 -> 8,8",
                "5,5 -> 8,2");

        return parseInput(inputs);
    }

    private List<Segment> givenDataset() {
        try (Stream<String> strings = Files.lines(Paths.get("src/test/resources/input.dataset"),
                StandardCharsets.UTF_8)) {
            return parseInput(strings.collect(toUnmodifiableList()));
        } catch (IOException ignored) {
        }

        return Collections.emptyList();
    }

    private List<Segment> parseInput(List<String> inputs) {
        return inputs.stream()
                .map(input -> {
                    String[] splittedInput = input.split(" -> ");

                    return new Segment(parseCoordinate(splittedInput[0]), parseCoordinate(splittedInput[1]));
                })
                .collect(Collectors.toList());
    }

    private Coordinate parseCoordinate(String input) {
        String[] split = input.split(",");

        return new Coordinate(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
    }
}