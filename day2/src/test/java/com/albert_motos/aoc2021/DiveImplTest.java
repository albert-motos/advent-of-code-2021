package com.albert_motos.aoc2021;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toUnmodifiableList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DiveImplTest {

    private Dive dive;

    @BeforeEach
    void setUp() {
        dive = new DiveImpl();
    }

    @DisplayName("GIVEN the sample dataset, WHEN multiplyFinalHorizontalByFinalDepth(), THEN result is the expected")
    @Test
    void multiplyFinalHorizontalByFinalDepthWithSampleDataset() {
        // Given
        List<String> dataset = givenSampleDataset();

        // When
        int actual = dive.multiplyFinalHorizontalByFinalDepth(dataset);

        // Then
        assertEquals(150, actual);
    }

    @DisplayName("GIVEN a dataset, WHEN multiplyFinalHorizontalByFinalDepth(), THEN result is the expected")
    @Test
    void multiplyFinalHorizontalByFinalDepthWithDataset() {
        // Given
        List<String> dataset = givenDataset();

        // When
        int actual = dive.multiplyFinalHorizontalByFinalDepth(dataset);

        // Then
        assertEquals(1383564, actual);
    }


    @DisplayName("GIVEN the sample dataset, WHEN multiplyFinalHorizontalByFinalDepthWithAim(), THEN result is the " +
            "expected")
    @Test
    void multiplyFinalHorizontalByFinalDepthWithAimWithSampleDataset() {
        // Given
        List<String> dataset = givenSampleDataset();

        // When
        int actual = dive.multiplyFinalHorizontalByFinalDepthWithAim(dataset);

        // Then
        assertEquals(900, actual);
    }

    @DisplayName("GIVEN a dataset, WHEN multiplyFinalHorizontalByFinalDepthWithAim(), THEN result is the expected")
    @Test
    void multiplyFinalHorizontalByFinalDepthWithAimWithDataset() {
        // Given
        List<String> dataset = givenDataset();

        // When
        int actual = dive.multiplyFinalHorizontalByFinalDepthWithAim(dataset);

        // Then
        assertEquals(1488311643, actual);
    }

    private List<String> givenSampleDataset() {
        return Arrays.asList("forward 5", "down 5", "forward 8", "up 3", "down 8", "forward 2");
    }

    private List<String> givenDataset() {
        try (Stream<String> strings = Files.lines(Paths.get("src/test/resources/input.dataset"),
                StandardCharsets.UTF_8)) {
            return strings.collect(toUnmodifiableList());
        } catch (IOException ignored) {
        }

        return null;
    }
}