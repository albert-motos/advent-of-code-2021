package com.albert_motos.aoc2021;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

public class DiveImpl implements Dive {

    private static final Pattern FORWARD_MOVEMENT = Pattern.compile("forward (\\d+)");
    private static final Pattern UP_MOVEMENT = Pattern.compile("up (\\d+)");
    private static final Pattern DOWN_MOVEMENT = Pattern.compile("down (\\d+)");

    @Override
    public int multiplyFinalHorizontalByFinalDepth(List<String> movements) {
        return multiplyFinalHorizontalByFinalDepth(movements, new DivePosition());
    }

    @Override
    public int multiplyFinalHorizontalByFinalDepthWithAim(List<String> movements) {
        return multiplyFinalHorizontalByFinalDepth(movements, new AdvancedDivePosition());
    }

    private int multiplyFinalHorizontalByFinalDepth(List<String> movements, DivePosition divePosition) {
        movements.forEach(movement -> {
            Matcher matcher;

            matcher = FORWARD_MOVEMENT.matcher(movement);
            if (matcher.find()) {
                divePosition.horizontalMovement(parseInt(matcher.group(1)));
            } else {
                matcher = UP_MOVEMENT.matcher(movement);
                if (matcher.find()) {
                    divePosition.verticalMovement(-parseInt(matcher.group(1)));
                } else {
                    matcher = DOWN_MOVEMENT.matcher(movement);
                    if (matcher.find()) {
                        divePosition.verticalMovement(parseInt(matcher.group(1)));
                    }
                }
            }
        });

        return divePosition.getDepth() * divePosition.getHorizontal();
    }

    private static class DivePosition {

        private int horizontal;
        private int depth;

        public DivePosition() {
            horizontal = 0;
            depth = 0;
        }

        public void horizontalMovement(int movement) {
            horizontal += movement;
        }

        public void verticalMovement(int movement) {
            depth += movement;
        }

        public int getHorizontal() {
            return horizontal;
        }

        public int getDepth() {
            return depth;
        }
    }

    private static class AdvancedDivePosition extends DivePosition {

        private int aim;

        public AdvancedDivePosition() {
            aim = 0;
        }

        @Override
        public void verticalMovement(int movement) {
            aim += movement;
        }

        @Override
        public void horizontalMovement(int movement) {
            super.horizontalMovement(movement);
            super.verticalMovement(movement * aim);
        }
    }
}
