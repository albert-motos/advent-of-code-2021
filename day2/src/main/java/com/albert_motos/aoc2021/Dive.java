package com.albert_motos.aoc2021;

import java.util.List;

public interface Dive {

    /**
     * Multiplies the final horizontal position by final depth after execute the list of movements.
     *
     * @param movements {@link List} of movements. Should match with that patter: forward X, down X or up X.
     * @return result of multiply the final horizontal position by the final depth.
     */
    int multiplyFinalHorizontalByFinalDepth(List<String> movements);

    /**
     * Multiplies the final horizontal position by final depth after execute the list of movements taking in care the
     * aim mechanic.
     *
     * @param movements {@link List} of movements. Should match with that patter: forward X, down X or up X.
     * @return result of multiply the final horizontal position by the final depth taking in care the aim mechanic.
     */
    int multiplyFinalHorizontalByFinalDepthWithAim(List<String> movements);
}
