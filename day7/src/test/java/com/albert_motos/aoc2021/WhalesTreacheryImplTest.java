package com.albert_motos.aoc2021;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WhalesTreacheryImplTest {

    private WhalesTreachery whalesTreachery;

    @BeforeEach
    void setUp() {
        whalesTreachery = new WhalesTreacheryImpl();
    }

    @DisplayName("GIVEN the sample dataset AND constant fuel consumption, WHEN getFuelForHorizontalAlignment(), THEN "
            + "result is the expected")
    @Test
    void getFuelForHorizontalAlignmentWithSampleDatasetAndConstantFuelConsumption() {
        // Given
        List<Integer> horizontalPositions = givenSampleDataset();
        WhalesTreachery.ConsumptionType consumptionType = WhalesTreachery.ConsumptionType.CONSTANT;

        // When
        int actual = whalesTreachery.getFuelForHorizontalAlignment(horizontalPositions, consumptionType);

        // Then
        assertEquals(37, actual);
    }

    @DisplayName("GIVEN a dataset AND constant fuel consumption, WHEN getFuelForHorizontalAlignment(), THEN result is" +
            " the expected")
    @Test
    void getFuelForHorizontalAlignmentWithDatasetAndConstantFuelConsumption() {
        // Given
        List<Integer> horizontalPositions = givenDataset();
        WhalesTreachery.ConsumptionType consumptionType = WhalesTreachery.ConsumptionType.CONSTANT;

        // When
        int actual = whalesTreachery.getFuelForHorizontalAlignment(horizontalPositions, consumptionType);

        // Then
        assertEquals(342730, actual);
    }

    @DisplayName("GIVEN the sample dataset AND linear fuel consumption, WHEN getFuelForHorizontalAlignment(), THEN " +
            "result is the expected")
    @Test
    void getFuelForHorizontalAlignmentWithSampleDatasetAndLinearFuelConsumption() {
        // Given
        List<Integer> horizontalPositions = givenSampleDataset();
        WhalesTreachery.ConsumptionType consumptionType = WhalesTreachery.ConsumptionType.LINEAR;

        // When
        int actual = whalesTreachery.getFuelForHorizontalAlignment(horizontalPositions, consumptionType);

        // Then
        assertEquals(168, actual);
    }

    @DisplayName("GIVEN a dataset AND linear fuel consumption, WHEN getFuelForHorizontalAlignment(), THEN result is " +
            "the expected")
    @Test
    void getFuelForHorizontalAlignmentWithDatasetAndLinearFuelConsumption() {
        // Given
        List<Integer> horizontalPositions = givenDataset();
        WhalesTreachery.ConsumptionType consumptionType = WhalesTreachery.ConsumptionType.LINEAR;

        // When
        int actual = whalesTreachery.getFuelForHorizontalAlignment(horizontalPositions, consumptionType);

        // Then
        assertEquals(92335207, actual);
    }

    private List<Integer> givenSampleDataset() {
        return parseInput("16,1,2,0,4,2,7,1,2,14");
    }

    private List<Integer> givenDataset() {
        try (Stream<String> strings = Files.lines(Paths.get("src/test/resources/input.dataset"),
                StandardCharsets.UTF_8)) {
            return parseInput(strings.collect(Collectors.joining()));
        } catch (IOException ignored) {
        }

        return Collections.emptyList();
    }

    private List<Integer> parseInput(String input) {
        return Arrays.stream(input.split(",")).map(Integer::valueOf).collect(Collectors.toList());
    }
}