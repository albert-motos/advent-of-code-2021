package com.albert_motos.aoc2021;

import java.util.List;

public interface WhalesTreachery {

    /**
     * Obtains the fuel spent for achieve the most expensive horizontal alignment of scraps positions.
     *
     * @param horizontalPositions {@link List} of Integer with horizontal scrap positions.
     * @param consumptionType     ConsumptionType
     * @return amount of fuel spend to achieve the most expensive horizontal alignment.
     */
    int getFuelForHorizontalAlignment(List<Integer> horizontalPositions, ConsumptionType consumptionType);

    enum ConsumptionType {
        CONSTANT,
        LINEAR
    }
}
