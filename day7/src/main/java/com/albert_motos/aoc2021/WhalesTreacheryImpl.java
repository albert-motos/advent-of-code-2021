package com.albert_motos.aoc2021;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WhalesTreacheryImpl implements WhalesTreachery {

    private final Map<Integer, Integer> linearFuelConsumption;

    public WhalesTreacheryImpl() {
        linearFuelConsumption = new HashMap<>();
    }


    @Override
    public int getFuelForHorizontalAlignment(List<Integer> horizontalPositions, ConsumptionType consumptionType) {
        if (horizontalPositions.isEmpty()) {
            return 0;
        }

        int position = horizontalPositions.stream()
                .mapToInt(i -> i)
                .sum() / horizontalPositions.size();
        int increment = 1;

        int fuelSpent = computeFuelConsumption(horizontalPositions, position, consumptionType);
        int fuelSpentInLowerPosition = computeFuelConsumption(horizontalPositions, position - 1, consumptionType);

        if (fuelSpentInLowerPosition < fuelSpent) {
            fuelSpent = fuelSpentInLowerPosition;
            position--;
            increment = -1;
        }

        boolean isLowerFuelSpent = false;
        while (!isLowerFuelSpent) {
            isLowerFuelSpent = true;
            int newFuelSpent = computeFuelConsumption(horizontalPositions, position + increment, consumptionType);

            if (newFuelSpent < fuelSpent) {
                position += increment;
                fuelSpent = newFuelSpent;
                isLowerFuelSpent = false;
            }
        }

        return fuelSpent;
    }

    private int computeFuelConsumption(List<Integer> horizontalPositions, final int position,
                                       ConsumptionType consumptionType) {
        if (ConsumptionType.CONSTANT.equals(consumptionType)) {
            return horizontalPositions.stream()
                    .mapToInt(i -> Math.abs(i - position))
                    .sum();
        }

        int totalFuelConsumption = 0;

        for (Integer horizontalPosition : horizontalPositions) {
            int movedPositions = Math.abs(horizontalPosition - position);
            Integer fuelConsumption = linearFuelConsumption.get(movedPositions);
            if (fuelConsumption == null) {
                fuelConsumption = fillFuelConsumption(movedPositions);
            }

            totalFuelConsumption += fuelConsumption;
        }

        return totalFuelConsumption;
    }

    private int fillFuelConsumption(int movedPositions) {
        if (movedPositions == 0) {
            return 0;
        }

        if (movedPositions == 1) {
            linearFuelConsumption.put(1, 1);
            return 1;
        }

        int fuelConsumption = fillFuelConsumption(movedPositions - 1) + movedPositions;
        linearFuelConsumption.put(movedPositions, fuelConsumption);

        return fuelConsumption;
    }
}
