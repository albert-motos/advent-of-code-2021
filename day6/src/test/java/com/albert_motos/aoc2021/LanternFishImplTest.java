package com.albert_motos.aoc2021;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LanternFishImplTest {

    private LanternFish lanternfish;

    @BeforeEach
    void setUp() {
        lanternfish = new LanternFishImpl();
    }

    @DisplayName("GIVEN the sample dataset AND a 80 days, WHEN getPopulation(), THEN result is the expected")
    @Test
    void getPopulationWithSampleDatasetAnd80Days() {
        // Given
        List<Integer> lanternFishTimers = givenSampleDataset();
        int days = 80;

        // When
        long actual = lanternfish.getPopulation(lanternFishTimers, days);

        // Then
        assertEquals(5934, actual);
    }

    @DisplayName("GIVEN a dataset AND a 80 days, WHEN getPopulation(), THEN result is the expected")
    @Test
    void getPopulationWithDatasetAnd80Days() {
        // Given
        List<Integer> lanternFishTimers = givenDataset();
        int days = 80;

        // When
        long actual = lanternfish.getPopulation(lanternFishTimers, days);

        // Then
        assertEquals(373378, actual);
    }

    @DisplayName("GIVEN the sample dataset AND a 256 days, WHEN getPopulation(), THEN result is the expected")
    @Test
    void getPopulationWithSampleDatasetAnd256Days() {
        // Given
        List<Integer> lanternFishTimers = givenSampleDataset();
        int days = 256;

        // When
        long actual = lanternfish.getPopulation(lanternFishTimers, days);

        // Then
        assertEquals(26984457539L, actual);
    }

    @DisplayName("GIVEN a dataset AND a 80 days, WHEN getPopulation(), THEN result is the expected")
    @Test
    void getPopulationWithDatasetAnd256Days() {
        // Given
        List<Integer> lanternFishTimers = givenDataset();
        int days = 256;

        // When
        long actual = lanternfish.getPopulation(lanternFishTimers, days);

        // Then
        assertEquals(1682576647495L, actual);
    }

    private List<Integer> givenSampleDataset() {
        return parseInput("3,4,3,1,2");
    }

    private List<Integer> givenDataset() {
        try (Stream<String> strings = Files.lines(Paths.get("src/test/resources/input.dataset"),
                StandardCharsets.UTF_8)) {
            return parseInput(strings.collect(Collectors.joining()));
        } catch (IOException ignored) {
        }

        return Collections.emptyList();
    }

    private List<Integer> parseInput(String input) {
        return Arrays.stream(input.split(",")).map(Integer::valueOf).collect(Collectors.toList());
    }
}