package com.albert_motos.aoc2021;

import java.util.List;

public interface LanternFish {

    /**
     * Obtains the population based on the originals' lantern fish timers after some time.
     *
     * @param lanternFishTimers {@link List} of lanterfish times.
     * @param days              specifies the time.
     * @return population of lanterfish after 80 days.
     */
    long getPopulation(List<Integer> lanternFishTimers, int days);
}
