package com.albert_motos.aoc2021;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LanternFishImpl implements LanternFish {

    @Override
    public long getPopulation(List<Integer> lanternFishTimers, int days) {
        Map<Integer, Long> state = IntStream.range(0, 8)
                .boxed()
                .collect(Collectors.toMap(Function.identity(), i -> 0L));

        lanternFishTimers.forEach(lanternFishTimer -> state.computeIfPresent(lanternFishTimer,
                (key, value) -> value + 1));

        for (int day = 0; day < days; day++) {
            Long newLanternFish = state.get(0);

            for (int i = 0; i < state.size() - 1; i++) {
                state.put(i, state.get(i + 1));
            }

            state.put(8, newLanternFish);
            state.computeIfPresent(6, (key, value) -> value + newLanternFish);
        }

        return state.values()
                .stream()
                .mapToLong(i -> i)
                .sum();
    }
}
