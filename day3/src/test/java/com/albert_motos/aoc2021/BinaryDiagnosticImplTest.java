package com.albert_motos.aoc2021;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toUnmodifiableList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BinaryDiagnosticImplTest {

    private BinaryDiagnostic binaryDiagnostic;

    @BeforeEach
    void setUp() {
        binaryDiagnostic = new BinaryDiagnosticImpl();
    }

    @DisplayName("GIVEN the sample dataset, WHEN getPowerConsumption(), THEN result is the expected")
    @Test
    void getPowerConsumptionWithSampleDataset() {
        // Given
        List<String> dataset = givenSampleDataset();

        // When
        int actual = binaryDiagnostic.getPowerConsumption(dataset);

        // Then
        assertEquals(198, actual);
    }

    @DisplayName("GIVEN a dataset, WHEN getPowerConsumption(), THEN result is the expected")
    @Test
    void getPowerConsumptionWithDataset() {
        // Given
        List<String> dataset = givenDataset();

        // When
        int actual = binaryDiagnostic.getPowerConsumption(dataset);

        // Then
        assertEquals(2035764, actual);
    }


    @DisplayName("GIVEN the sample dataset, WHEN getLifeSupportRating(), THEN result is the expected")
    @Test
    void getLifeSupportRatingWithSampleDataset() {
        // Given
        List<String> dataset = givenSampleDataset();

        // When
        int actual = binaryDiagnostic.getLifeSupportRating(dataset);

        // Then
        assertEquals(230, actual);
    }

    @DisplayName("GIVEN a dataset, WHEN getLifeSupportRating(), THEN result is the expected")
    @Test
    void getLifeSupportRatingWithDataset() {
        // Given
        List<String> dataset = givenDataset();

        // When
        int actual = binaryDiagnostic.getLifeSupportRating(dataset);

        // Then
        assertEquals(2817661, actual);
    }

    private List<String> givenSampleDataset() {
        return Arrays.asList("00100", "11110", "10110", "10111", "10101", "01111", "00111", "11100", "10000", "11001"
                , "00010", "01010");
    }

    private List<String> givenDataset() {
        try (Stream<String> strings = Files.lines(Paths.get("src/test/resources/input.dataset"),
                StandardCharsets.UTF_8)) {
            return strings.collect(toUnmodifiableList());
        } catch (IOException ignored) {
        }

        return null;
    }
}