package com.albert_motos.aoc2021;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BinaryDiagnosticImpl implements BinaryDiagnostic {


    @Override
    public int getPowerConsumption(List<String> diagnosticReports) {
        if (diagnosticReports.isEmpty()) {
            return 0;
        }

        int reportLength = diagnosticReports.get(0).length();
        List<Integer> sumByPosition = IntStream.range(0, reportLength)
                .map(i -> 0)
                .boxed()
                .collect(Collectors.toList());

        for (String diagnosticReport : diagnosticReports) {
            for (int i = 0; i < reportLength; i++) {
                sumByPosition.set(i, sumByPosition.get(i) + ('0' == diagnosticReport.charAt(i) ? 0 : 1));
            }
        }

        String mostCommonBits = sumByPosition.stream()
                .map(sum -> sum > diagnosticReports.size() / 2 ? "1" : "0")
                .collect(Collectors.joining(""));

        int gammaRate = Integer.parseInt(mostCommonBits, 2);

        return (int) (gammaRate * (Math.pow(2, reportLength) - gammaRate - 1));
    }

    @Override
    public int getLifeSupportRating(List<String> diagnosticReports) {
        String numberWithMostCommonBit = getNumberWithCommonBitOnComparison(diagnosticReports, 0,
                (sum, middle) -> sum >= middle);
        String numberWithLessCommonBit = getNumberWithCommonBitOnComparison(diagnosticReports, 0,
                (sum, middle) -> sum < middle);
        return Integer.parseInt(numberWithMostCommonBit, 2) * Integer.parseInt(numberWithLessCommonBit, 2);
    }

    private String getNumberWithCommonBitOnComparison(List<String> numbers, int index,
                                                      BiPredicate<Integer, Double> comparisonFunction) {
        if (numbers.size() == 1) {
            return numbers.get(0);
        }

        List<String> zerosNumbers = new ArrayList<>();
        List<String> onesNumbers = new ArrayList<>();
        int sum = 0;

        for (String number : numbers) {
            if ('0' == number.charAt(index)) {
                zerosNumbers.add(number);
            } else {
                onesNumbers.add(number);
                sum++;
            }
        }

        return getNumberWithCommonBitOnComparison(comparisonFunction.test(sum, numbers.size() / 2.0) ? onesNumbers :
                zerosNumbers, index + 1, comparisonFunction);
    }
}
