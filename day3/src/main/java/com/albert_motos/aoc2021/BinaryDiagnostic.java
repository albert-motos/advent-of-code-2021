package com.albert_motos.aoc2021;

import java.util.List;

public interface BinaryDiagnostic {

    /**
     * Obtains the power consumption of the submarine, multiplying the gamma rate by the epsilon rate.
     * - Gamma rate is computed finding the most common bit on each position of the diagnosticReports.
     * - Epsilon rate is computed finding the less common bit on each position of the diagnosticReports.
     *
     * @param diagnosticReports {@link List} of String. Contains a binary strings.
     * @return result of multiply the gamma rate by the epsilon rate.
     */
    int getPowerConsumption(List<String> diagnosticReports);

    /**
     * Obtains the life support rate of the submarine, multiplying the oxygen generator rating by the CO2 scrubber
     * rating.
     * - Oxygen generator rating is the result of find the numbers with the most common bit on first position, then
     * discard the others. Repeat that with next bit when finally, only one number is remaining. If the ones and
     * zeros are equally, pick the ones numbers.
     * - CO2 scrubber rating is the result of find the number with the less common bit on first position, then discard
     * the others. Repeat that with the next bit until finally, only one number is remaining. If the ones and zeros are
     * equally, pick the zero numbers.
     *
     * @param diagnosticReports {@link List} of String. Contains a binary strings.
     * @return result of multiply the oxygen generator rating by the CO2 scrubber rating.
     */
    int getLifeSupportRating(List<String> diagnosticReports);
}
