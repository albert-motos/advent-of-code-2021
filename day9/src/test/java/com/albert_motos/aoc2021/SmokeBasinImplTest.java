package com.albert_motos.aoc2021;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SmokeBasinImplTest {

    private SmokeBasin smokeBasin;

    @BeforeEach
    void setUp() {
        smokeBasin = new SmokeBasinImpl();
    }

    @DisplayName("GIVEN the sample dataset, WHEN sumOfRiskLevelsOfLowPoints(), THEN result is the expected")
    @Test
    void sumOfRiskLevelsOfLowPointsWithSampleDataset() {
        // Given
        Integer[][] heightMap = givenSampleDataset();

        // When
        int actual = smokeBasin.sumOfRiskLevelsOfLowPoints(heightMap);

        // Then
        assertEquals(15, actual);
    }

    @DisplayName("GIVEN a dataset, WHEN sumOfRiskLevelsOfLowPoints(), THEN result is the expected")
    @Test
    void sumOfRiskLevelsOfLowPointsWithDataset() {
        // Given
        Integer[][] heightMap = givenDataset();

        // When
        int actual = smokeBasin.sumOfRiskLevelsOfLowPoints(heightMap);

        // Then
        assertEquals(452, actual);
    }


    @DisplayName("GIVEN the sample dataset, WHEN multiplySizesOfThreeLargestBasins(), THEN result is the expected")
    @Test
    void multiplySizesOfThreeLargestBasinsWithSampleDataset() {
        // Given
        Integer[][] heightMap = givenSampleDataset();

        // When
        int actual = smokeBasin.multiplySizesOfThreeLargestBasins(heightMap);

        // Then
        assertEquals(1134, actual);
    }

    @DisplayName("GIVEN a dataset, WHEN multiplySizesOfThreeLargestBasins(), THEN result is the expected")
    @Test
    void multiplySizesOfThreeLargestBasinsWithDataset() {
        // Given
        Integer[][] heightMap = givenDataset();

        // When
        int actual = smokeBasin.multiplySizesOfThreeLargestBasins(heightMap);

        // Then
        assertEquals(1263735, actual);
    }

    private Integer[][] givenSampleDataset() {
        List<String> inputs = Arrays.asList("2199943210", "3987894921", "9856789892", "8767896789", "9899965678");
        return parseInput(inputs);
    }

    private Integer[][] givenDataset() {
        try (Stream<String> strings = Files.lines(Paths.get("src/test/resources/input.dataset"),
                StandardCharsets.UTF_8)) {
            return parseInput(strings.collect(Collectors.toList()));
        } catch (IOException ignored) {
        }

        return null;
    }

    private Integer[][] parseInput(List<String> rawInputs) {
        return rawInputs.stream().map(rawInput -> {
            Integer[] row = new Integer[rawInput.length()];
            for (int i = 0; i < rawInput.length(); i++) {
                row[i] = Integer.parseInt(String.valueOf(rawInput.charAt(i)));
            }

            return row;
        }).toArray(Integer[][]::new);
    }
}