package com.albert_motos.aoc2021;

public interface SmokeBasin {

    /**
     * Obtains the sum of the risk levels of all low points.
     * <p>
     * Low point is defined a point with lower value on the orthogonal neighbours. Cases in border are also considered.
     * Risk level is defined as low point value +1.
     *
     * @param heightMap Integer matrix with the heights on each position. 0 means lower and 9 higher.
     * @return Sum of risk levels from lower height points of the matrix.
     */
    int sumOfRiskLevelsOfLowPoints(Integer[][] heightMap);

    /**
     * Multiplies the size of the three largest basins.
     * <p>
     * A basin is all locations that eventually flow downward to a single low point.
     *
     * @param heightMap Integer matrix with the heights on each position. 0 means lower and 9 higher.
     * @return Result of multiply the size of the three largest basins.
     */
    int multiplySizesOfThreeLargestBasins(Integer[][] heightMap);
}
