package com.albert_motos.aoc2021;

import java.util.*;

public class SmokeBasinImpl implements SmokeBasin {

    @Override
    public int sumOfRiskLevelsOfLowPoints(Integer[][] heightMap) {
        int riskLevel = 0;

        for (int row = 0; row < heightMap.length; row++) {
            for (int column = 0; column < heightMap[row].length; column++) {
                if ((row - 1 < 0 || heightMap[row - 1][column] > heightMap[row][column])
                        && (heightMap[row].length - 1 < column + 1 || heightMap[row][column + 1] > heightMap[row][column])
                        && (heightMap.length - 1 < row + 1 || heightMap[row + 1][column] > heightMap[row][column])
                        && (column - 1 < 0 || heightMap[row][column - 1] > heightMap[row][column])) {
                    riskLevel += heightMap[row][column] + 1;
                }
            }
        }

        return riskLevel;
    }

    @Override
    public int multiplySizesOfThreeLargestBasins(Integer[][] heightMap) {
        List<Set<Integer>> basins = new ArrayList<>();

        for (int row = 0; row < heightMap.length; row++) {
            addNewBasins(heightMap, basins, row);
        }

        return basins.stream()
                .sorted(Comparator.comparing(Set::size, Comparator.reverseOrder()))
                .limit(3)
                .mapToInt(Set::size)
                .reduce(1, (a, b) -> a * b);
    }

    private void addNewBasins(Integer[][] heightMap, List<Set<Integer>> basins, int row) {
        Set<Integer> basin = new HashSet<>();
        int belongsToBasin = -1;

        for (int column = 0; column < heightMap[row].length; column++) {
            if (heightMap[row][column] == 9) {
                if (!basin.isEmpty()) {
                    saveBasin(belongsToBasin, basins, basin);

                    basin = new HashSet<>();
                    belongsToBasin = -1;
                }
            } else {
                if (row > 0 && heightMap[row - 1][column] != 9) {
                    belongsToBasin = getBelongsToBasin(basins, belongsToBasin,
                            (row - 1) * heightMap[row].length + column);
                }

                basin.add(row * heightMap[row].length + column);
            }
        }

        if (!basin.isEmpty()) {
            saveBasin(belongsToBasin, basins, basin);
        }
    }

    private int getBelongsToBasin(List<Set<Integer>> basins, int belongsToBasin, int neighbour) {
        int i = 0;
        int newBelongsToBasin = -1;

        while (newBelongsToBasin == -1 && i < basins.size()) {
            if (basins.get(i).contains(neighbour)) {
                newBelongsToBasin = i;
            } else {
                i++;
            }
        }

        if (newBelongsToBasin != belongsToBasin) {
            if (belongsToBasin == -1) {
                belongsToBasin = newBelongsToBasin;
            } else if (newBelongsToBasin != -1) {
                basins.get(newBelongsToBasin).addAll(basins.get(belongsToBasin));
                belongsToBasin = newBelongsToBasin;
            }
        }
        return belongsToBasin;
    }

    private void saveBasin(int belongsToBasin, List<Set<Integer>> basins, Set<Integer> basin) {
        if (belongsToBasin == -1) {
            basins.add(basin);
        } else {
            basins.get(belongsToBasin).addAll(basin);
        }
    }
}
