# Advent of Code - 2021

Solutions of the [following](https://adventofcode.com/2021) challenges.
