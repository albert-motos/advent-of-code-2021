package com.albert_motos.aoc2021;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SevenSegmentSearchImplTest {

    private SevenSegmentSearch sevenSegmentSearch;

    @BeforeEach
    void setUp() {
        sevenSegmentSearch = new SevenSegmentSearchImpl();
    }

    @DisplayName("GIVEN the sample dataset, WHEN getDistinguishedDigitsInOutput(), THEN result is the expected")
    @Test
    void getDistinguishedDigitsInOutputWithSampleDataset() {
        // Given
        List<DisplayValue> displayValues = givenSampleDataset();

        // When
        int actual = sevenSegmentSearch.getDistinguishedDigitsInOutput(displayValues);

        // Then
        assertEquals(26, actual);
    }

    @DisplayName("GIVEN a dataset, WHEN getDistinguishedDigitsInOutput(), THEN result is the expected")
    @Test
    void getDistinguishedDigitsInOutputWithDataset() {
        // Given
        List<DisplayValue> displayValues = givenDataset();

        // When
        int actual = sevenSegmentSearch.getDistinguishedDigitsInOutput(displayValues);

        // Then
        assertEquals(342, actual);
    }

    @DisplayName("GIVEN the sample dataset, WHEN outputSum(), THEN result is the expected")
    @Test
    void outputSumWithSampleDataset() {
        // Given
        List<DisplayValue> displayValues = givenSampleDataset();

        // When
        int actual = sevenSegmentSearch.outputSum(displayValues);

        // Then
        assertEquals(61229, actual);
    }

    @DisplayName("GIVEN a dataset, WHEN outputSum(), THEN result is the expected")
    @Test
    void outputSumWithDataset() {
        // Given
        List<DisplayValue> displayValues = givenDataset();

        // When
        int actual = sevenSegmentSearch.outputSum(displayValues);

        // Then
        assertEquals(1068933, actual);
    }

    private List<DisplayValue> givenSampleDataset() {
        List<String> inputs = Arrays.asList(
                "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe",
                "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc",
                "fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg",
                "fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb",
                "aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea",
                "fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb",
                "dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe",
                "bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef",
                "egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb",
                "gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce");
        return parseInput(inputs);
    }

    private List<DisplayValue> givenDataset() {
        try (Stream<String> strings = Files.lines(Paths.get("src/test/resources/input.dataset"),
                StandardCharsets.UTF_8)) {
            return parseInput(strings.collect(Collectors.toList()));
        } catch (IOException ignored) {
        }

        return Collections.emptyList();
    }

    private List<DisplayValue> parseInput(List<String> rawInputs) {
        return rawInputs.stream()
                .map(rawInput -> {
                    String[] split = rawInput.split(" \\| ");
                    List<String> inputs = Arrays.stream(split[0].split(" ")).collect(Collectors.toUnmodifiableList());
                    List<String> outputs = Arrays.stream(split[1].split(" ")).collect(Collectors.toUnmodifiableList());

                    return new DisplayValue(inputs, outputs);
                })
                .collect(Collectors.toUnmodifiableList());
    }
}