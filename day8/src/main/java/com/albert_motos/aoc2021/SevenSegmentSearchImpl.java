package com.albert_motos.aoc2021;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public class SevenSegmentSearchImpl implements SevenSegmentSearch {

    // There are the equivalent of numbers 1, 4, 7 and 8 in seven-segment display
    private static final Set<Integer> DISTINGUISHED_NUMBERS_SIZE = Set.of(2, 4, 3, 7);

    @Override
    public int getDistinguishedDigitsInOutput(List<DisplayValue> displayValues) {
        return displayValues.stream()
                .map(DisplayValue::getOutput)
                .flatMap(List::stream)
                .filter(output -> DISTINGUISHED_NUMBERS_SIZE.contains(output.length()))
                .mapToInt(i -> 1)
                .sum();
    }

    @Override
    public int outputSum(List<DisplayValue> displayValues) {
        return displayValues.stream()
                .mapToInt(this::decodeOutput)
                .sum();
    }

    private int decodeOutput(DisplayValue displayValue) {
        Optional<String> one = displayValue.getInput().stream().filter(s -> s.length() == 2).findFirst();
        Optional<String> four = displayValue.getInput().stream().filter(s -> s.length() == 4).findFirst();

        if (one.isEmpty() || four.isEmpty()) {
            return 0;
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (String output : displayValue.getOutput()) {
            switch (output.length()) {
                case 2:
                    stringBuilder.append("1");
                    break;
                case 4:
                    stringBuilder.append("4");
                    break;
                case 3:
                    stringBuilder.append("7");
                    break;
                case 7:
                    stringBuilder.append("8");
                    break;
                case 5:
                    if (amountOfSegmentsAfterDiff(output, one.get()) == 3) {
                        stringBuilder.append("3");
                    } else if (amountOfSegmentsAfterDiff(output, four.get()) == 3) {
                        stringBuilder.append("2");
                    } else {
                        stringBuilder.append("5");
                    }
                    break;
                case 6:
                    if (amountOfSegmentsAfterDiff(output, one.get()) == 5) {
                        stringBuilder.append("6");
                    } else if (amountOfSegmentsAfterDiff(output, four.get()) == 3) {
                        stringBuilder.append("0");
                    } else {
                        stringBuilder.append("9");
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Unexpected size: " + output.length());
            }
        }

        return Integer.parseInt(stringBuilder.toString());
    }

    private int amountOfSegmentsAfterDiff(String s, String segments) {
        int amount = s.length();

        for (int i = 0; i < s.length(); i++) {
            if (segments.contains(String.valueOf(s.charAt(i)))) {
                amount--;
            }
        }

        return amount;
    }
}
