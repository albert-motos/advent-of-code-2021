package com.albert_motos.aoc2021;

import java.util.List;

public class DisplayValue {

    private final List<String> input;
    private final List<String> output;

    public DisplayValue(List<String> input, List<String> output) {
        this.input = input;
        this.output = output;
    }

    public List<String> getInput() {
        return input;
    }

    public List<String> getOutput() {
        return output;
    }
}
