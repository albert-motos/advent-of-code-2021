package com.albert_motos.aoc2021;

import java.util.List;

public interface SevenSegmentSearch {

    /**
     * Obtains how many times the numbers 1, 4, 7 or 8 appears on the output of displayValues.
     *
     * @param displayValues {@link List} of {@link DisplayValue} with the output to analyze.
     * @return how many times the number 1, 4 ,7 or 8 appears on the output of displayValues.
     */
    int getDistinguishedDigitsInOutput(List<DisplayValue> displayValues);

    /**
     * Obtains the sum of each output value after decode the numbers.
     *
     * @param displayValues {@link List} of {@link DisplayValue} with values to analize.
     * @return sum of decoded output values.
     */
    int outputSum(List<DisplayValue> displayValues);
}
