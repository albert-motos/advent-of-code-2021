package com.albert_motos.aoc2021;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class GiantSquidImpl implements GiantSquid {

    @Override
    public int getFinalScoreOfWinningBoard(List<Integer> numbers, List<Integer[][]> rawBoards) {
        List<Board> boards = rawBoards.stream()
                .map(Board::new)
                .collect(Collectors.toList());

        for (Integer number : numbers) {
            try {
                for (Board board : boards) {
                    board.mark(number);
                }
            } catch (WinnerNumberException e) {
                return number * e.getBoard().getUnmarkedNumbers()
                        .stream()
                        .mapToInt(i -> i)
                        .sum();
            }
        }

        return 0;
    }

    @Override
    public int getFinalScoreOfLastWinningBoard(List<Integer> numbers, List<Integer[][]> rawBoards) {
        List<Board> boards = rawBoards.stream()
                .map(Board::new)
                .collect(Collectors.toList());

        for (Integer number : numbers) {
            List<Board> toBeRemoved = new ArrayList<>();
            for (Board board : boards) {
                try {
                    board.mark(number);
                } catch (WinnerNumberException e) {
                    if (boards.size() - toBeRemoved.size() == 1) {
                        return number * e.getBoard().getUnmarkedNumbers()
                                .stream()
                                .mapToInt(i -> i)
                                .sum();
                    }

                    toBeRemoved.add(e.getBoard());
                }
            }

            boards.removeAll(toBeRemoved);
        }

        return 0;
    }

    static class Board implements Serializable {

        private final Map<Integer, List<Integer>> numberLocationsByNumber;
        private final List<Set<Integer>> numbers;

        public Board(Integer[][] rawBoard) {
            numberLocationsByNumber = new HashMap<>();
            numbers = new ArrayList<>();

            for (int i = 0; i < rawBoard.length; i++) {
                Set<Integer> columnSet = new HashSet<>();
                Set<Integer> rowSet = new HashSet<>();
                for (int j = 0; j < rawBoard[0].length; j++) {
                    rowSet.add(rawBoard[i][j]);
                    numberLocationsByNumber.compute(rawBoard[i][j], (key, value) -> {
                        if (value == null) {
                            value = new ArrayList<>();
                        }
                        value.add(numbers.size());

                        return value;
                    });

                    columnSet.add(rawBoard[j][i]);
                    numberLocationsByNumber.compute(rawBoard[j][i], (key, value) -> {
                        if (value == null) {
                            value = new ArrayList<>();
                        }
                        value.add(numbers.size() + 1);

                        return value;
                    });
                }

                numbers.add(rowSet);
                numbers.add(columnSet);
            }


        }

        public Set<Integer> getUnmarkedNumbers() {
            return numberLocationsByNumber.keySet();
        }

        public void mark(Integer number) throws WinnerNumberException {
            List<Integer> numberLocations = numberLocationsByNumber.remove(number);
            if (numberLocations != null) {
                for (Integer numberLocation : numberLocations) {
                    Set<Integer> lineOfNumbers = numbers.get(numberLocation);
                    if (lineOfNumbers.remove(number)
                            && lineOfNumbers.isEmpty()) {
                        throw new WinnerNumberException(this);
                    }
                }
            }
        }
    }

    static class WinnerNumberException extends Exception {

        private final Board board;

        WinnerNumberException(Board board) {
            this.board = board;
        }

        public Board getBoard() {
            return board;
        }
    }
}
