package com.albert_motos.aoc2021;

import java.util.List;

public interface GiantSquid {

    /**
     * Obtains the final score from the winning board, adding all numbers not marked and multiplying it by the number
     * that was just called.
     *
     * @param numbers {@link List} of Integer with called numbers in order.
     * @param boards  {@link List} of Board structure, form by a 5x5 matrix of Integers.
     * @return result of multiply the last called number with the sum of all unmarked numbers of winning board.
     */
    int getFinalScoreOfWinningBoard(List<Integer> numbers, List<Integer[][]> boards);

    /**
     * Obtains the final score from the last winning board, adding all numbers not marked and multiplying it by the
     * number that was just called.
     *
     * @param numbers {@link List} of Integer with called numbers in order.
     * @param boards  {@link List} of Board structure, form by a 5x5 matrix of Integers.
     * @return result of multiply the last called number with the sum of all unmarked numbers of last winning board.
     */
    int getFinalScoreOfLastWinningBoard(List<Integer> numbers, List<Integer[][]> boards);
}
