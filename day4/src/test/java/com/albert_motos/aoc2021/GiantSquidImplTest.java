package com.albert_motos.aoc2021;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toUnmodifiableList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GiantSquidImplTest {

    private GiantSquid giantSquid;

    @BeforeEach
    void setUp() {
        giantSquid = new GiantSquidImpl();
    }

    @DisplayName("GIVEN the sample dataset, WHEN getFinalScoreOfWinningBoard(), THEN result is the expected")
    @Test
    void getFinalScoreOfWinningBoardWithSampleDataset() {
        // Given
        List<Integer> numbers = new ArrayList<>();
        List<Integer[][]> boards = new ArrayList<>();
        givenSampleDataset(numbers, boards);

        // When
        int actual = giantSquid.getFinalScoreOfWinningBoard(numbers, boards);

        // Then
        assertEquals(4512, actual);
    }

    @DisplayName("GIVEN a dataset, WHEN getFinalScoreOfWinningBoard(), THEN result is the expected")
    @Test
    void getFinalScoreOfWinningBoardWithDataset() {
        // Given
        List<Integer> numbers = new ArrayList<>();
        List<Integer[][]> boards = new ArrayList<>();
        givenDataset(numbers, boards);

        // When
        int actual = giantSquid.getFinalScoreOfWinningBoard(numbers, boards);

        // Then
        assertEquals(41503, actual);
    }

    @DisplayName("GIVEN the sample dataset, WHEN getFinalScoreOfLastWinningBoard(), THEN result is the expected")
    @Test
    void getFinalScoreOfLastWinningBoardWithSampleDataset() {
        // Given
        List<Integer> numbers = new ArrayList<>();
        List<Integer[][]> boards = new ArrayList<>();
        givenSampleDataset(numbers, boards);

        // When
        int actual = giantSquid.getFinalScoreOfLastWinningBoard(numbers, boards);

        // Then
        assertEquals(1924, actual);
    }

    @DisplayName("GIVEN a dataset, WHEN getFinalScoreOfLastWinningBoard(), THEN result is the expected")
    @Test
    void getFinalScoreOfLastWinningBoardWithDataset() {
        // Given
        List<Integer> numbers = new ArrayList<>();
        List<Integer[][]> boards = new ArrayList<>();
        givenDataset(numbers, boards);

        // When
        int actual = giantSquid.getFinalScoreOfLastWinningBoard(numbers, boards);

        // Then
        assertEquals(3178, actual);
    }

    private void givenSampleDataset(List<Integer> numbers, List<Integer[][]> boards) {
        List<String> inputs = Arrays.asList(
                "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1",
                "",
                "22 13 17 11  0",
                "8  2 23  4 24",
                "21  9 14 16  7",
                "6 10  3 18  5",
                "1 12 20 15 19",
                "",
                "3 15  0  2 22",
                "9 18 13 17  5",
                "19  8  7 25 23",
                "20 11 10 24  4",
                "14 21 16 12  6",
                "",
                "14 21 17 24  4",
                "10 16 15  9 19",
                "18  8 23 26 20",
                "22 11 13  6  5",
                "2  0 12  3  7"
        );

        parseInput(inputs, numbers, boards);
    }

    private void givenDataset(List<Integer> numbers, List<Integer[][]> boards) {
        try (Stream<String> strings = Files.lines(Paths.get("src/test/resources/input.dataset"),
                StandardCharsets.UTF_8)) {
            parseInput(strings.collect(toUnmodifiableList()), numbers, boards);
        } catch (IOException ignored) {
        }

    }

    private void parseInput(List<String> inputs, List<Integer> numbers, List<Integer[][]> boards) {
        numbers.addAll(Arrays.stream(inputs.get(0).split(","))
                .map(Integer::valueOf)
                .collect(Collectors.toList()));

        Integer[][] board = new Integer[5][5];
        int row = 0;
        for (String input : inputs.subList(2, inputs.size())) {
            if (input.isEmpty()) {
                boards.add(board);
                board = new Integer[5][5];
                row = 0;
            } else {
                board[row++] = Arrays.stream(input.split(" "))
                        .filter(s -> !s.isEmpty()).map(Integer::valueOf).toArray(Integer[]::new);
            }
        }

        boards.add(board);
    }
}